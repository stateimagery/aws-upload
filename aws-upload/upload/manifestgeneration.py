""" Common functions used across manifest generation process

Author: Nicholas Kershaw (nicholas.kershaw@resources.qld.gov.au)
User: State Imagery
Software Version: Python 3.7
Date Created: 21/09/2020
Last Edited: 01/09/2021
"""

from pathlib import Path
import pandas as pd
from datetime import datetime as dt
import numpy as np
import traceback
import base64
import math
import hashlib
import time
import json
import importlib.resources
import calendar

MANIFEST_VERSION = "2020-09-03"


class ManifestFile:
    def __init__(self, source_directory: str, s3bucket_name: str, storage_config_name: str) -> None:
        with importlib.resources.path("aws-upload.storageclass_specs", storage_config_name) as storage_config_path:
            with open(storage_config_path, 'r') as storage_config_json:
                storage_config_data = json.load(storage_config_json)
        self.default_storage_class = storage_config_data['default_class']
        self.file_storage_classes = storage_config_data['storage_classes']
        self.source_directory = source_directory
        self.destination_bucket = s3bucket_name
        self.version = MANIFEST_VERSION
        self.manifest_creation_time = dt.now().strftime("%Y-%m-%d")
        self.files_details = []
        self.commit_files = []

    def add_file(self, path: str, file: str, size: int, b64md5_checksum, md5_chunks: dict, key: str) -> None:
        """
        Generate details of file: path, file, size in bytes and base64 MD5 checksum
        Args:
            path: path to file to add to dictionary
            file: file name with extension to add to dictionary
            size: file size in bytes
            b64md5_checksum: Base65 encoded MD5 checksum to add to dictionary
            md5_chunks: dictionary of file 5 gigabyte chunk MD5s
            key: AWS key for file

        Returns: Dictionary including generated details

        """

        file_dict = {'path': path, 'file': file, 'size': size, 'key': key}
        if file.endswith('.tmp') is False:
            if any(class_key in str(Path(path).joinpath(file)) for class_key in self.file_storage_classes.keys()):
                for class_key in self.file_storage_classes:
                    if class_key in str(Path(path).joinpath(file)):
                        file_dict['storageClass'] = self.file_storage_classes[class_key]
                        break
            else:
                file_dict['storageClass'] = self.default_storage_class

            file_dict['b64md5checksum'] = b64md5_checksum
            if md5_chunks:
                file_dict['b64md5checksum_parts'] = md5_chunks
            self.files_details.append(file_dict)

    def get_manifest_dict(self) -> dict:
        return {'sourcePath': self.source_directory, 'destination': self.destination_bucket,
                'version': self.version,
                'creationTimestamp': str(calendar.timegm(time.gmtime())),
                'files': self.files_details}

    def write_manifest_json(self, write_path: str, split_num: int, log_details: dict, manifest_id: str = None) -> None:
        """
        Write manifest json from class variables. Can be split into multiple manifests based on multi session upload
        requirements
        Args:
            write_path: Path to directory to write json
            split_num: Number of manifest jsons to split data into
            manifest_id: String to easily identify manifest file
            log_details: log_details: Dictionary including json logging object and name

        """
        manifest_cat = Misc.get_manifest_cat(self.source_directory)

        json_log = log_details['json_logger']

        for i, sub_file_list in enumerate(np.array_split(self.files_details, split_num)):
            manifest_contents = self.get_manifest_dict()
            manifest_contents['files'] = sub_file_list.tolist()
            if manifest_id is not None:
                manifest_file_name = f'{self.manifest_creation_time}_{self.destination_bucket}_{manifest_cat}_{manifest_id}_manifest.json'
            else:
                manifest_file_name = f'{self.manifest_creation_time}_{self.destination_bucket}_{manifest_cat}_manifest.json'

            if split_num > 1:
                manifest_file_name = f'{Path(manifest_file_name).stem}_{i + 1}.json'

            # Attempt to write file up to 3 times
            write_path_name = Path(write_path).joinpath(manifest_file_name)
            write_attempt_count = 0
            while write_attempt_count < 3:
                write_attempt_count += 1
                try:
                    with open(write_path_name, 'w') as manifest_json_file:
                        json.dump(manifest_contents, manifest_json_file, indent=4, default=str)
                    json_log.add_summary_check(f'Write Attempt {write_attempt_count} of 3',
                                               f'Success: {write_path_name}')
                    break
                except (PermissionError, FileNotFoundError) as e:
                    json_log.add_summary_check(f'Write Attempt {write_attempt_count} of 3',
                                               f'Failed to write to {write_path_name}: {e}')
                    # If write failed, move one folder up directory structure
                    write_path = Path(write_path).parents[0]
                    write_path_name = Path(write_path).joinpath(manifest_file_name)
            else:
                json_log.add_summary_check(f'Write failed', f'Failed to write after {write_attempt_count} attempts')

    def __str__(self):
        return str({'sourcePath': self.source_directory, 'destination': self.destination_bucket,
                    'version': self.version,
                    'creationTimestamp': str(calendar.timegm(time.gmtime())),
                    'files': self.files_details})


class ManGen:
    @staticmethod
    def create_part_planet_keys(planet_files_sorted: dict) -> dict:
        """ Create AWS key by extracting details from metadata JSON """
        part_item_id_aws_keys = {}
        for dict_key in planet_files_sorted:
            # Open relevant JSON file
            try:
                with open(str([s for s in planet_files_sorted[dict_key] if s.endswith('.json')][0]),
                          'r') as item_json:
                    json_content = json.load(item_json)
                    item_json.close()

                # Process coordinates
                coordinates_list = json_content['geometry']['coordinates']
                if len(coordinates_list) == 1:
                    corners_arr = np.asarray(json_content['geometry']['coordinates'][0][0:4])
                else:
                    corners_arr = np.asarray(json_content['geometry']['coordinates'][0][0][0:4])

                length = corners_arr.shape[0]
                sum_x = np.sum(corners_arr[:, 0])
                sum_y = np.sum(corners_arr[:, 1])
                lat, long = int(sum_x / length), abs(int(sum_y / length))

                # Process year & month
                year, month = str(json_content['properties']['acquired']).split('-')[0:2]

                # Process item type
                item_type = json_content['properties']['item_type']
                if 'psscene' in item_type.lower() or 'psortho' in item_type.lower():
                    sensor_name = 'planetscope'
                elif 'rescene' in item_type.lower() or 'reortho' in item_type.lower():
                    sensor_name = 'rapideye'
                else:
                    continue

                # Generate key
                lat_long_sub = '{}_{}'.format(int(lat), int(abs(long)))
                new_folder = str(Path(year).joinpath(month, lat_long_sub).as_posix())
                part_item_id_aws_keys[dict_key] = str(
                    Path('satellite').joinpath('sensor', sensor_name, new_folder).as_posix())
            except IndexError:
                print(f'{dict_key} no json file found')

        return part_item_id_aws_keys

    @staticmethod
    def create_file_aws_part_key(source_path: str, file_path: str, root_key: str, single_proj_bool: bool) -> str:
        """
        Create aws key without file for files other than planet
        Args:
            source_path: String source path of files
            file_path: String file path excluding file
            root_key: Root aws key e.g. aerial/2019
            single_proj_bool: Boolean if creating manifest for single project

        Returns: String AWS key with prefix/year/key

        """
        source_path = str(Path(source_path).as_posix())
        file_path = str(Path(file_path).as_posix())

        if single_proj_bool:
            source_path = str(Path(source_path).parent.as_posix())
        main_key = file_path.replace(source_path, '').lstrip('/')
        return str(Path(root_key).joinpath(main_key).as_posix())

    @staticmethod
    def _pop_man_file(aws_part_key: str, file_loc: str, is_snowball: bool) -> list:
        """
        Generate details for manifest file through multiprocessing
        Args:
            aws_part_key: File AWS key excluding file name
            file_loc: Path to file to populate details
            is_snowball: If process is being conducted for snowball

        Returns: Local file path, file name, size in bytes, b64md5 checksum, 64md5 checksum for 5 gb parts, AWS key path

        """

        gb = 1024 ** 3
        mb = 1024 ** 2

        start_time = dt.now()

        if is_snowball:
            max_upload_size = 512 * mb
            read_parts_chunks = 2
        else:
            max_upload_size = 5 * gb
            read_parts_chunks = 20

        file_name = Path(file_loc).name  # Generate file path & file name
        aws_key = Path(aws_part_key).joinpath(file_name).as_posix()  # Generate full AWS key

        try:

            size = Path(file_loc).stat().st_size
            chunks = {}

            # Generate MD5 checksum
            hash_md5 = hashlib.md5()
            hash_md5_chunks = hashlib.md5()
            chunk_counter = 1
            counter = 0

            parts = math.ceil(size / (256 * mb))
            with open(file_loc, "rb") as f:
                for file_part in iter(lambda: f.read(256 * mb), b""):  # read in as binary
                    hash_md5.update(file_part)
                    # Generate checksums for chunks if file > max upload size
                    # Chunks generated from reading file in 512 megabyte chunks to not overuse memory
                    if size > max_upload_size:
                        hash_md5_chunks.update(file_part)
                        parts -= 1
                        counter += 1
                        if counter >= read_parts_chunks:
                            chunks[chunk_counter] = base64.b64encode(hash_md5_chunks.digest()).decode('utf-8')
                            chunk_counter += 1
                            counter = 0
                            hash_md5_chunks = hashlib.md5()
                        elif parts == 0:
                            chunks[chunk_counter] = base64.b64encode(hash_md5_chunks.digest()).decode('utf-8')
                            chunk_counter += 1

            f.close()
            checksum = base64.b64encode(hash_md5.digest()).decode('utf-8')

            end_time = dt.now()

            manifest_details = {'runtime': end_time - start_time, 'file_size': size, 'nchunks': chunk_counter}

            return [Path(file_loc).parent, file_name, size, checksum, chunks, aws_key, 'generated',
                    manifest_details]
        except Exception as e:
            print("ERROR -", ' '.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__)))
            return [Path(file_loc).parent, file_name, None, None, None, None, 'failed',
                    f"ERROR - {' '.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))}"]


class Misc:
    @staticmethod
    def get_manifest_cat(source_path: str) -> str:
        """
        Defines manifest category (Planet, SISP, 3D, Misc) from MANIFEST categories
        Args:
            source_path: Path to determine category

        Returns: Manifest category

        """
        try:
            return [cat for cat in ["Planet", "3D", "SISP"] if cat.lower() in source_path.lower()][0]
        except IndexError:
            return 'Misc'

    @staticmethod
    def concat_dataframes(df_paths: list) -> pd.DataFrame:
        """
        Extract AWS file keys from AWS inventory csvs
        Args:
            df_paths: AWS inventory csvs

        Returns: AWS files contained within inventory csv/csvs

        """
        if len(df_paths) == 1:
            return pd.read_csv(df_paths[0], compression='gzip',
                               names=["Bucket", "Key", "VersionId", "IsLatest", "IsDeleteMarker", "Size",
                                      "LastModifiedDate", "ETag", "StorageClass", "IsMultipartUploaded",
                                      "ObjectLockRetainUntilDate", "ObjectLockMode", "ObjectLockLegalHoldStatus"],
                               dtype={"Bucket": "object", "Key": "object", "VersionId": "object", "IsLatest": "bool",
                                      "IsDeleteMarker": "bool", "Size": "float64", "LastModifiedDate": "object",
                                      "ETag": "object", "StorageClass": "object", "IsMultipartUploaded": "object",
                                      "ObjectLockRetainUntilDate": "object", "ObjectLockMode": "object",
                                      "ObjectLockLegalHoldStatus": "object"}
                               )
        elif len(df_paths) > 1:
            inv_dfs = []
            for csv in df_paths:
                inv_dfs.append(pd.read_csv(csv, compression='gzip',
                                           names=["Bucket", "Key", "VersionId", "IsLatest", "IsDeleteMarker", "Size",
                                                  "LastModifiedDate", "ETag", "StorageClass", "IsMultipartUploaded",
                                                  "ObjectLockRetainUntilDate", "ObjectLockMode",
                                                  "ObjectLockLegalHoldStatus"],
                                           dtype={"Bucket": "object", "Key": "object", "VersionId": "object", "IsLatest": "bool",
                                                  "IsDeleteMarker": "bool", "Size": "float64", "LastModifiedDate": "object",
                                                  "ETag": "object", "StorageClass": "object", "IsMultipartUploaded": "object",
                                                  "ObjectLockRetainUntilDate": "object", "ObjectLockMode": "object",
                                                  "ObjectLockLegalHoldStatus": "object"}))
            return pd.concat(inv_dfs)
