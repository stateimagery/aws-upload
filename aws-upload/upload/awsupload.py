""" Common functions used across s3 upload process

Author: Nicholas Kershaw (nicholas.kershaw@resources.qld.gov.au)
User: State Imagery
Software Version: Python 3.7
Date Created: 21/09/2020
Last Edited: 13/07/2021
"""

from pathlib import Path
from datetime import datetime as dt
from botocore.exceptions import ClientError
from boto3 import client
import json
import math

GB = 1024 ** 3
MB = 1024 ** 2
NEWLINE_TAB = '\n\t\t\t\t\t'


class Upload:
    @staticmethod
    def put_object_single(aws_client: client, bucket_name: str, upload_file_path: str, key: str, b64md5: str,
                          storage_class: str, log_details: dict, snowball: bool = False) -> bool:
        """
        Read file in binary and upload to aws using boto3 put object function
        Verifies upload using base64 encoded MD5 checksum
        Args:
            aws_client: AWS client object
            bucket_name: Name of S3 bucket where data will be uploaded
            upload_file_path: File path of file to upload
            key: Pre-generated S3 key for file in S3
            b64md5: Base64 MD4 checksum to verify upload integrity
            storage_class: AWS S3 storage class e.g. STANDARD or GLACIER
            log_details: Dictionary including python logging object and json logging object
            snowball: Boolean to remove storage class for snowball uploads

        Returns: False if upload failed

        """

        logger = log_details['logger']
        json_logger = log_details['json_logger']
        log_file_name = log_details['file_name']

        try:
            start_dt = dt.now()
            with open(upload_file_path, 'rb') as file_content:
                logger.info(f'UPLOADING - {upload_file_path}')

                if snowball:
                    response = aws_client.put_object(Bucket=bucket_name, Body=file_content, Key=key, ContentMD5=b64md5)
                else:
                    response = aws_client.put_object(Bucket=bucket_name, Body=file_content, Key=key, ContentMD5=b64md5,
                                                     StorageClass=storage_class)
            try:
                response_etag = json.loads(response['ETag'])
            except TypeError:
                response_etag = response['ETag']

            logger.info(
                f'SUCCESS:{NEWLINE_TAB}Upload - {upload_file_path}{NEWLINE_TAB}Bucket - {bucket_name}{NEWLINE_TAB}Key - {key}'
                f'{NEWLINE_TAB}Response ETag - {response_etag}')

            upload_details = {"runtime": dt.now() - start_dt, "checksum": b64md5,
                              "aws": {"bucket": bucket_name, "key": key,
                                      "etag": response_etag}}
            json_logger.add_object_process(log_file_name, 'upload', 'success', upload_details)

        except ClientError as e:
            json_logger.add_object_process(log_file_name, 'upload', 'fail', e)
            logger.error(f'FAILED: upload {upload_file_path}')
            logger.error(e)
            return False
        except FileNotFoundError:
            json_logger.add_object_process(log_file_name, 'upload', 'fail', 'file not found')
            logger.error(f'FAILED: File not found {upload_file_path}')
            return False

    @staticmethod
    def put_object_chunks(aws_client: client, bucket_name: str, upload_file_path: str, key: str, b64md5: str,
                          part_dict: dict, storage_class: str, log_details: dict, snowball: bool = False) -> bool:
        """
        Read file in binary chunks and upload to aws using boto3 multipart upload functions
        Verifies upload using base64 encoded MD5 checksum
        Args:
            aws_client: AWS client object
            bucket_name: Name of S3 bucket where data will be uploaded
            upload_file_path: File path of file to upload
            key: Pre-generated S3 key for file in S3
            b64md5: Base64 MD4 checksum to verify upload integrity
            part_dict: Dictionary of 5gb chunk MD5
            storage_class: AWS S3 storage class e.g. STANDARD or GLACIER
            log_details: Dictionary including python logging object and json logging object
            snowball: Boolean to remove storage class for snowball uploads

        Returns: False if upload failed

        """

        logger = log_details['logger']
        json_logger = log_details['json_logger']
        log_file_name = log_details['file_name']

        if snowball:
            multipart_upload = aws_client.create_multipart_upload(Bucket=bucket_name, Key=key)
            upload_size = 512 * MB
        else:
            multipart_upload = aws_client.create_multipart_upload(Bucket=bucket_name, Key=key,
                                                                  StorageClass=storage_class)
            upload_size = 5 * GB

        try:
            # If parts do not exist for file over max upload size raise exception
            if part_dict is None:
                raise Exception(f'b64md5checksum_parts do not exist in manifest for {upload_file_path}')

            num_chunks = math.ceil(Path(upload_file_path).stat().st_size / upload_size)
            logger.info(f'{upload_file_path} > max upload size, uploading in parts')

            part_num = 1

            parts_info = []

            # Load file in chunks
            start_dt = dt.now()
            logger.info(f'UPLOADING - {upload_file_path}')
            with open(upload_file_path, 'rb') as file_content:
                for chunk in iter(lambda: file_content.read(upload_size), b""):
                    chunk_md5 = part_dict[str(part_num)]

                    part_response = aws_client.upload_part(Body=chunk, Bucket=bucket_name, Key=key,
                                                           UploadId=multipart_upload['UploadId'],
                                                           PartNumber=part_num,
                                                           ContentMD5=chunk_md5)

                    try:
                        part_response_etag = json.loads(part_response['ETag'])
                    except TypeError:
                        part_response_etag = part_response['ETag']

                    parts_info.append({'ETag': part_response_etag, 'PartNumber': part_num})
                    logger.info(
                        f'SUCCESS:{NEWLINE_TAB}Upload {upload_file_path} part {part_num} of {num_chunks}{NEWLINE_TAB}Bucket - {bucket_name}{NEWLINE_TAB}Key - {key}'
                        f'{NEWLINE_TAB}Chunk MD5 - {chunk_md5}{NEWLINE_TAB}Response ETag - {part_response_etag}')
                    part_num += 1

            response = aws_client.complete_multipart_upload(Bucket=bucket_name, Key=key,
                                                            UploadId=multipart_upload['UploadId'],
                                                            MultipartUpload={'Parts': parts_info})

            try:
                response_etag = json.loads(response['ETag'])
            except TypeError:
                response_etag = response['ETag']

            logger.info(
                f'SUCCESS:{NEWLINE_TAB}Upload - {upload_file_path}{NEWLINE_TAB}Bucket - {bucket_name}{NEWLINE_TAB}Key - {key}'
                f'{NEWLINE_TAB}Response ETag - {response_etag}')

            upload_details = {"runtime": dt.now() - start_dt, "checksum": b64md5, "num_chunks": num_chunks,
                              "checksum_chunks": part_dict,
                              "aws": {"bucket": bucket_name, "key": key, "etag": response_etag}}
            json_logger.add_object_process(log_file_name, 'upload', 'success', upload_details)

        except ClientError as e:
            json_logger.add_object_process(log_file_name, 'upload', 'fail', e)
            logger.error(f'FAILED: upload {upload_file_path}')
            logger.error(e)
            aws_client.abort_multipart_upload(
                Bucket=bucket_name,
                Key=key,
                UploadId=multipart_upload['UploadId']
            )
            return False
        except FileNotFoundError:
            json_logger.add_object_process(log_file_name, 'upload', 'fail', 'file not found')
            logger.error(f'FAILED: File not found {upload_file_path}')
            aws_client.abort_multipart_upload(
                Bucket=bucket_name,
                Key=key,
                UploadId=multipart_upload['UploadId']
            )
            return False

    @staticmethod
    def logs_upload(aws_client: client, upload_file_path: str, category: str, up_dir: str, log_details: dict,
                    bucket_name: str) -> bool:
        """
        Upload manifest files and upload logs to AWS logs bucket
        Args:
            aws_client: AWS client object
            upload_file_path: Path to file to be uploaded
            category: File category (SISP, 3D, Planet, Misc)
            up_dir: Name of directory above category (manifests, upload-logs)
            bucket_name: Name of AWS bucket
            log_details: Dictionary including python logging object and json logging object

        Returns: False if upload failed

        """

        logger = log_details['logger']
        json_logger = log_details['json_logger']

        upload_file_name = Path(upload_file_path).name
        json_logger.add_object(upload_file_name, upload_file_path)

        key = str(Path(f'upload-workflow/{up_dir}/{category}/').joinpath(
            upload_file_name))
        start_dt = dt.now()
        try:
            aws_client.upload_file(upload_file_path, bucket_name, key)
            upload_details = {"runtime": dt.now() - start_dt, "aws": {"bucket": bucket_name, "key": key}}
            json_logger.add_object_process(upload_file_name, 'upload', 'success', upload_details)
            logger.info(f'UPLOADED - {upload_file_path} to {key}')
        except Exception as e:
            json_logger.add_object_process(upload_file_name, 'upload', 'fail', e)
            logger.error(f'FAILED - {upload_file_path} to {key}')
            logger.error(e)
            return False


class Validation:
    @staticmethod
    def key_exists(aws_client: client, bucket: str, key: str) -> bool:
        """
        Search for key in AWS bucket to determine if exists
        Args:
            aws_client: AWS client object
            bucket: AWS bucket name
            key: AWS object key

        Returns (bool): True of False depending of object exists

        """

        response = aws_client.list_objects_v2(
            Bucket=bucket,
            Prefix=key,
        )
        for obj in response.get('Contents', []):
            if obj['Key'] == key:
                if obj['Size'] is None:
                    return False
                else:
                    return True
        return False


class Misc:
    @staticmethod
    def get_manifest_cat(source_path: str) -> str:
        """
        Defines manifest category (Planet, SISP, 3D, Misc) from MANIFEST categories
        Args:
            source_path: Path to determine category

        Returns: Manifest category

        """
        try:
            return [cat for cat in ["Planet", "3D", "SISP"] if cat.lower() in source_path.lower()][0]
        except IndexError:
            return 'Misc'
