"""
Manages logs of object processing and validation

Author: John Tasker (john.tasker@resources.qld.gov.au)
User: State Imagery
Software Version: Python 3.8+
Date Created: 7/7/21
Last Edited: 8/7/21
"""

import importlib
import os
import pathlib
import sys
import json
import traceback
import types
import typing
import stdlib_list
import collections
import functools
from importlib import metadata
from datetime import datetime, timedelta

PYTHON_STDLIB_VERSION = f'{sys.version_info.major}.{sys.version_info.minor}'
STDLIB_MODULES = stdlib_list.stdlib_list(PYTHON_STDLIB_VERSION)

DecoratorReturn = collections.namedtuple('DecoratorReturn',
                                         ['object_id',
                                          'file_path',
                                          'func_name',
                                          'status',
                                          'details',
                                          'func_return'])


def get_module_name(module: typing.Union[str, types.ModuleType]):
    try:
        return module.__name__
    except AttributeError:
        return module


class Dependency:
    """Stores basic metadata for a Python library dependency"""

    @classmethod
    def from_module(cls, module: types.ModuleType):
        try:
            name = module.__package__
            if name is None:
                return
            version = metadata.version(name)
        except metadata.PackageNotFoundError:
            name = module.__name__.split('.')[0]
            version = metadata.version(name)

        submodules = []
        if module.__name__ != name:
            submodules.append(module.__name__)
        return cls(name, version, submodules)

    def __init__(self, name: str, version: str = None, submodules: typing.List[str] = None):
        self.name = name
        self.version = version
        self._submodules = set(submodules)

    def has_submodule(self, module: typing.Union[str, types.ModuleType]):
        return get_module_name(module) in self.submodules

    @property
    def submodules(self):
        return list(self._submodules)

    @submodules.setter
    def submodules(self, submodules: typing.List[typing.Union[str, types.ModuleType]]):
        self._submodules = set([get_module_name(module) for module in submodules])

    def add_submodule(self, module: typing.Union[str, types.ModuleType]):
        self.submodules = self.submodules + [module]

    def get_list(self):
        output = [self.name, self.version]
        if self.submodules:
            output.append(self.submodules)
        return output


class DependencyList:
    """Collates Python library dependencies."""

    @classmethod
    def from_parent_module(cls, module: types.ModuleType):
        modules = []
        for obj in module.__dict__.values():
            try:
                if obj.__package__:
                    module_name = obj.__name__
                else:
                    continue
            except AttributeError:
                try:
                    module_name = obj.__module__
                except AttributeError:
                    continue
            if module_name not in STDLIB_MODULES and not module_name.startswith('_'):
                modules.append(module_name)
        imported_modules = [importlib.import_module(module) for module in set(modules)]
        return cls(imported_modules)

    def __init__(self, modules: typing.List[types.ModuleType]):
        self.depends = []
        for module in modules:
            dependency = Dependency.from_module(module)
            self.add_dependency(dependency)

    def has_dependency(self, dependency):
        for depends in self.depends:
            if dependency.name == depends.name:
                return True
        return False

    def get_dependency(self, name):
        for depends in self.depends:
            if name == depends.name:
                return depends
        raise ValueError(f'No dependency matching \'{name}\' found in dependency list')

    def add_dependency(self, dependency):
        if not self.has_dependency(dependency):
            self.depends.append(dependency)
        else:
            depend = self.get_dependency(dependency.name)
            for submodule in dependency.submodules:
                depend.add_submodule(submodule)

    def get_list(self):
        return [depend.get_list() for depend in self.depends]


class Logging:
    """ Defines and manipulates log object """

    CHECK = 'check'
    PROCESS = 'process'

    CHECK_KEY = 'checks'
    PROCESS_KEY = 'processes'

    PASS = 'pass'
    FAIL = 'fail'

    def __init__(self):
        """ Initialises Logging Class """
        self.log = {
            "date": str(datetime.now()),
            "script": {
                "name": "",
                "version": "",
                "dependencies": {
                    "aws_upload": {
                        "version": metadata.version("aws_upload"),
                        "modules": ["aws_upload.logs"]}
                },
                "variables": {},
                "environment": {
                    "platform": sys.platform,
                    "python": sys.version
                }
            },
            "summary": {
                "checks": {},
                "processes": {}
            },
            "objects": {}
        }

    def add_script_bulk(self, script_name, script_version, dependency_list, variable_list):
        self.add_script_name(script_name)
        self.add_script_version(script_version)
        self.add_script_dependency_bulk(dependency_list)
        self.add_script_variable_bulk(variable_list)

    def add_script_name(self, script_name: str):
        self.log["script"]["name"] = script_name

    def add_script_version(self, script_version: str):
        self.log["script"]["version"] = script_version

    def add_script_dependency_bulk(self, dependency_list: list):
        for dependency in dependency_list:
            if len(dependency) > 2:
                self.add_script_dependency(dependency[0], dependency[1], dependency[2])
            else:
                self.add_script_dependency(dependency[0], dependency[1])

    def add_script_dependency(self, name, version, modules=None):
        if modules is not None:
            self.log["script"]["dependencies"][name] = {"version": version, "modules": modules}
        else:
            self.log["script"]["dependencies"][name] = {"version": version}

    def set_script_dependency_version(self, name: str, version: str):
        self.log["script"]["dependencies"][name]["version"] = version

    def set_script_dependency_modules(self, name: str, modules: list):
        self.log["script"]["dependencies"][name]["modules"] = modules

    def append_script_dependency_module(self, name: str, module: str):
        self.log["script"]["dependencies"][name]["modules"].append(module)

    def add_script_variable(self, key, value):
        self.log["script"]["variables"][key] = value

    def add_script_variable_bulk(self, variable_list: list):
        for variable in variable_list:
            self.add_script_variable(variable[0], variable[1])

    def add_summary_check(self, key, value):
        self.log["summary"]["checks"][key] = value

    def add_summary_process(self, key, value):
        self.log["summary"]["processes"][key] = value

    def summarise_task(self, task_key, task_name, summary=None):
        if summary is not None:
            self.log['summary'][task_key][task_name] = summary
            return

        counts = collections.defaultdict(int)
        errors = collections.defaultdict(list)
        counts.update({'total': 0,
                       self.PASS: 0,
                       self.FAIL: 0})
        total_run_time = timedelta()
        for _id, obj in self.log['objects'].items():
            try:
                task = obj[task_key][task_name]
                counts['total'] += 1
                counts[task['status']] += 1
                try:
                    total_run_time += task['details']['run_time']
                except KeyError:
                    pass
                try:
                    error = task['details']['error']
                    errors[error].append(_id)
                except KeyError:
                    pass
            except KeyError:
                pass
        new_summary = dict(counts)
        new_summary.update({'total_run_time': total_run_time, 'errors': errors})
        self.summarise_task(task_key, task_name, new_summary)

    def summarise_tasks(self, task_keys=None):
        if task_keys is None:
            task_keys = [self.CHECK_KEY, self.PROCESS_KEY]

        for task_key in task_keys:
            task_names = set()
            for obj in self.log['objects'].values():
                task_names.update(obj[task_key].keys())

            for task_name in task_names:
                self.summarise_task(task_key, task_name)

    def add_object_details_bulk(self, obj_list):
        for obj in obj_list:
            self.add_object(obj[0], obj[1])

    def add_object(self, obj_id, obj_path, exist_ok=True):
        if not self.log["objects"].get(obj_id):
            self.log["objects"][obj_id] = {"path": obj_path, "checks": {}, "processes": {}}
            return
        if not exist_ok:
            raise ValueError(f'Object with key \'${obj_id}\' already exists')

    def add_object_bulk(self, obj_list):
        for obj in obj_list:
            self.add_object(obj[0], obj[1])

    def set_obj_path(self, obj_id, obj_path):
        self.log["objects"][obj_id]["path"] = obj_path

    def add_object_check(self, obj_id: str, check_name: str, check_status: str, check_details=None):
        if check_details is not None:
            self.log["objects"][obj_id]["checks"][check_name] = {"status": check_status, "details": check_details}
        else:
            self.log["objects"][obj_id]["checks"][check_name] = {"status": check_status}

    def set_object_check_status(self, obj_id: str, check_name: str, check_status: str):
        self.log["objects"][obj_id]["checks"][check_name]["status"] = check_status

    def set_object_check_details(self, obj_id: str, check_name: str, check_details: str):
        self.log["objects"][obj_id]["checks"][check_name]["details"] = check_details

    def add_object_process(self, obj_id: str, process_name: str, process_status: str, process_details=None):
        if process_details is not None:
            self.log["objects"][obj_id]["processes"][process_name] = {"status": process_status,
                                                                      "details": process_details}
        else:
            self.log["objects"][obj_id]["processes"][process_name] = {"status": process_status}

    def set_object_process_status(self, obj_id, process_name, process_status):
        self.log["objects"][obj_id]["processes"][process_name]["status"] = process_status

    def set_object_process_details(self, obj_id, process_name, process_details):
        self.log["objects"][obj_id]["processes"][process_name]["details"] = process_details

    def output_json(self, out_path: str, out_file: str):
        try:
            if not out_file.endswith(".json"):
                out_file = f"{out_file.split('.')[0]}.json"
            out_file_path = os.path.join(out_path, out_file)
        except Exception as e:
            print(f"ERROR - {' '.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))}")
            sys.exit(1)

        with open(out_file_path, "w") as log_json:
            json.dump(self.log, log_json, indent=4, default=str)

        print(f"\nLog Exported: {out_file_path}\n")


def log_task(timer: bool = True, full_path_id: bool = False,
                   id_kwarg: str = None, arg_pos: int = None):
    def log_task_decorator(func):
        @functools.wraps(func)
        def log_task_wrapper(_file: typing.Union[str, pathlib.Path],
                             *args, **kwargs) -> DecoratorReturn:
            if arg_pos is not None:
                _file = args[arg_pos]
            elif id_kwarg is not None:
                _file = kwargs.get(id_kwarg, _file)
            else:
                _file = _file
            file_path = pathlib.Path(_file)
            object_id = str(file_path) if full_path_id else file_path.name
            start_time = datetime.now()
            try:
                func_return = func(_file, *args, **kwargs)
                success, extra_details = func_return
                status = Logging.PASS if success else Logging.FAIL
                details = {}
                try:
                    details.update(extra_details)
                except TypeError:
                    pass
            except Exception as e:
                func_return = None
                status = Logging.FAIL
                details = {'error': str(e)}

            run_time = datetime.now() - start_time
            if timer:
                details['run_time'] = run_time

            return DecoratorReturn(object_id, file_path, func.__name__, status, details, func_return)

        return log_task_wrapper

    return log_task_decorator


def log_task_result(logger: Logging, task_type: str, result: DecoratorReturn):
    object_id, file_path, func_name, status, details, func_return = result
    logger.add_object(object_id, file_path)
    log_func = getattr(logger, f'add_object_{task_type}')
    log_func(object_id, func_name, status, details)
