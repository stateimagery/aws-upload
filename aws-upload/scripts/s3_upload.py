""" Read manifest json file details and upload to specified AWS bucket using links provided inside the manifest such as:
- File path of file to upload
- Generated AWS file key
- Base64 encoded MD5 checksum for verification during upload
- Storage class for file in AWS (Standard, Glacier, etc)

Author: Nicholas Kershaw (nicholas.kershaw@resources.qld.gov.au)
User: State Imagery
Software Version: Python 3.7
Date Created: 10/09/2020
Last Edited: 13/07/2021
"""
import boto3
import click
from datetime import datetime as dt
import json
from pathlib import Path
from tqdm import tqdm
from importlib import metadata
import logging
import fnmatch
import aws_upload.logs
from aws_upload.upload.awsupload import Misc, Upload, GB, MB, Validation


def _logging_setup(log_path: str, log_name: str) -> logging.Logger:
    log_file_path = Path(log_path).joinpath(f'{log_name}.log')
    log = logging.getLogger(log_name)
    log.setLevel(logging.INFO)
    file_handler = logging.FileHandler(log_file_path)
    formatter = logging.Formatter('%(asctime)s.%(msecs)03d - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    file_handler.setFormatter(formatter)
    log.addHandler(file_handler)
    return log


@click.command()
@click.argument('manifest_path', nargs=1, type=click.Path(exists=True))
@click.argument('aws_profile', nargs=1, type=click.STRING)
@click.option('-client', '--aws_client', nargs=1, type=click.STRING, default='s3')
@click.option('-region', '--aws_region', nargs=1, type=click.STRING, default=None)
@click.option('-endpoint', '--aws_endpoint', nargs=1, type=click.STRING, default=None)
@click.option('-log', '--log_path', nargs=1, type=click.Path(exists=True), default='/scratch/lsi/aws_upload_logs/')
def main(manifest_path, aws_profile, aws_client, aws_region, aws_endpoint, log_path):
    if aws_region == 'snow':
        log_path = '/scratch/lsi/snowball_upload_logs/'

    # Logging setup
    logger = _logging_setup(log_path, Path(manifest_path).stem)

    # JSON logging setup
    json_log_name = f"{Path(manifest_path).stem}-log.json"
    json_log = aws_upload.logs.Logging()
    dependency_list = [["aws_upload", metadata.version("aws_upload"), ["aws_upload.logs", "aws_upload.upload.s3_upload"]],
                       ["click", metadata.version("click")], ["boto3", metadata.version("boto3")],
                       ["tqdm", metadata.version("tqdm")]]
    variable_list = [["manifest_path", manifest_path], ["aws_profile", aws_profile]]
    json_log.add_script_bulk("s3_upload.py", metadata.version("aws_upload"), dependency_list, variable_list)

    # Combine logging details into dictionary to pass into functions
    log_input = {
        "logger": logger,
        "json_logger": json_log,
        "file_name": ""
    }

    process_start = dt.now()
    logger.info("Processing Started - {}\n".format(process_start))

    failed_upload_files = []
    processed_files = []

    boto3.setup_default_session(profile_name=aws_profile)
    if aws_region == 'snow':
        aws_client = boto3.client(service_name=aws_client,
                                  region_name=aws_region,
                                  endpoint_url=aws_endpoint)
    else:
        aws_client = boto3.client(service_name=aws_client)

    if aws_region == 'snow':
        max_upload_size = 512 * MB
    else:
        max_upload_size = 5 * GB

    logger.debug('Reading {}'.format(manifest_path))
    with open(manifest_path, 'r') as manifest_json:
        manifest_data = json.load(manifest_json)
        s3bucket_name = manifest_data['destination']
        for file_details in tqdm(manifest_data['files']):
            # Extract file details
            file_path = file_details['path']
            file_name = file_details['file']
            upload_file = Path(file_path).joinpath(file_name)
            file_size = file_details['size']

            if file_name not in processed_files:
                json_log.add_object(file_name, upload_file)
                processed_files.append(file_name)
                log_input['file_name'] = file_name
            else:
                i = 1
                file_name_inc = f'Dup {i}: {file_name}'
                while file_name_inc in processed_files:
                    i += 1
                    file_name_inc = f'Dup {i}: {file_name}'
                else:
                    json_log.add_object(file_name_inc, upload_file)
                    processed_files.append(file_name_inc)
                    log_input['file_name'] = file_name_inc

            aws_key = file_details['key']  # Extract aws key
            file_b64md5 = file_details['b64md5checksum']  # Extract base 64 encoded md5 checksum
            try:
                b64md5_chunks = file_details['b64md5checksum_parts']
            except KeyError:
                b64md5_chunks = None
            storage_class = file_details['storageClass']  # Extract aws storageClass for file

            # Upload of data through put object
            if file_size > max_upload_size:  # Upload data in chunks if over max upload size
                status = Upload.put_object_chunks(aws_client, s3bucket_name, upload_file, aws_key, file_b64md5,
                                                  b64md5_chunks,
                                                  storage_class, log_input,
                                                  snowball=True if aws_region == 'snow' else False)
            else:  # Upload data whole if under max upload size
                status = Upload.put_object_single(aws_client, s3bucket_name, upload_file, aws_key, file_b64md5,
                                                  storage_class, log_input,
                                                  snowball=True if aws_region == 'snow' else False)
            if status is False:
                failed_upload_files.append(upload_file)

    process_end = dt.now()
    logger.info("Processing Ended - {}".format(process_end))
    logger.info("Processing Time - {}".format(process_end - process_start))

    category = Misc.get_manifest_cat(manifest_path)

    # Object upload validation
    num_files_man = len(manifest_data['files'])
    upload_count = 0

    for manifest_obj in list(
            dict.fromkeys([(manifest_obj['path'], manifest_obj['file'], manifest_obj['key']) for
                           manifest_obj in manifest_data['files']])):
        for filter_file in fnmatch.filter(processed_files, f"*{manifest_obj[1]}"):
            if Validation.key_exists(aws_client, s3bucket_name, manifest_obj[2]):
                json_log.add_object_check(filter_file, 'verify_upload', 'pass')
                upload_count += 1
            else:
                json_log.add_object_check(filter_file, 'verify_upload', 'fail')
                if Path(manifest_obj[0]).joinpath(manifest_obj[1]) not in failed_upload_files:
                    failed_upload_files.append(Path(manifest_obj[0]).joinpath(manifest_obj[1]))

    json_log.add_summary_process('files_upload',
                                 {'upload_runtime': process_end - process_start, 'total': num_files_man,
                                  'upload_pass': upload_count, 'upload_fail': num_files_man - upload_count,
                                  'failed_uploads': failed_upload_files})
    if aws_region != 'snow':
        aux_up_failed = []
        aux_upload_count = 0

        aux_start = dt.now()
        # Upload manifest file to archive logs bucket
        # TODO - Update bucket for log information
        man_up_status = Upload.logs_upload(aws_client, manifest_path, category, 'manifests', log_input)
        if man_up_status is False:
            aux_up_failed.append(manifest_path)
        else:
            aux_upload_count += 1
        json_log.add_summary_process('aux_upload', {'upload_runtime': dt.now() - aux_start, 'total': 1,
                                                    'upload_pass': aux_upload_count,
                                                    'upload_fail': 1 - aux_upload_count,
                                                    'failed_uploads': aux_up_failed})

        # Validate manifest upload
        # TODO - Update bucket for archive-logs
        if Validation.key_exists(aws_client, 'archive-logs',
                                 str(Path(f'upload-workflow/manifests/{category}/').joinpath(
                                     str(Path(manifest_path).name)))):
            json_log.add_object_check(str(Path(manifest_path).name), 'verify_upload', 'pass')
        else:
            json_log.add_object_check(str(Path(manifest_path).name), 'verify_upload', 'fail')
            if manifest_path not in aux_up_failed:
                aux_up_failed.append(manifest_path)

        json_log.output_json(log_path, json_log_name)

        # Upload log file to archive logs bucket
        log_up_status = Upload.logs_upload(aws_client, str(Path(log_path).joinpath(json_log_name)), category,
                                           'upload-logs',
                                           log_input)

        # Validate log upload
        if Validation.key_exists(aws_client, 'archive-logs',
                                 str(Path(f'upload-workflow/upload-logs/{category}/').joinpath(
                                     json_log_name))):
            json_log.add_object_check(json_log_name, 'verify_upload', 'pass')
        else:
            json_log.add_object_check(json_log_name, 'verify_upload', 'fail')
            if str(Path(log_path).joinpath(json_log_name)) not in aux_up_failed:
                aux_up_failed.append(str(Path(log_path).joinpath(json_log_name)))

        if log_up_status is False:
            aux_up_failed.append(str(Path(log_path).joinpath(json_log_name)))
        else:
            aux_upload_count += 1
        json_log.add_summary_process('aux_upload', {'upload_runtime': dt.now() - aux_start, 'total': 2,
                                                    'upload_pass': aux_upload_count,
                                                    'upload_fail': 2 - aux_upload_count,
                                                    'failed_uploads': aux_up_failed})
    json_log.output_json(log_path, json_log_name)


if __name__ == '__main__':
    main()
