""" Produces file containing information for reference when uploading to AWS S3

Script to generate AWS style manifest JSON to store pre-generated file information
prior to uploading to AWS S3. Details included:
- File Source
- File Destination
- File size in bytes
- Base64 encoded MD5 checksum to load during put-object --content-md5 tag execution

Author: Nicholas Kershaw (nicholas.kershaw@resources.qld.gov.au) & John Tasker
User: State Imagery
Software Version: Python 3.7
Date Created: 03/09/2020
Last Edited: 01/09/2021
"""

from os import walk
from pathlib import Path
import click
import pandas as pd
import multiprocessing as mp
from importlib import metadata
from datetime import datetime as dt
import aws_upload.logs
from aws_upload.upload.manifestgeneration import Misc, ManGen, ManifestFile


def _input_validation(root_aws_key: str, storage_class_json: str) -> str:
    """
    Check script inputs for errors and make corrections or throw exception
    Args:
        root_aws_key: Input from main to be checked
        storage_class_json: Input from main to be checked

    Returns: Corrected root aws key

    """

    root_aws_key = Path(root_aws_key).as_posix()
    if '\\' in root_aws_key:
        raise Exception(f'root_aws_key input argument invalid: {root_aws_key}')
    if root_aws_key.endswith('/'):
        root_aws_key = root_aws_key[:-1]
    if root_aws_key.startswith('/'):
        root_aws_key = root_aws_key[1:]

    if not storage_class_json.endswith('.json'):
        raise Exception(f'storage_class_json input argument is not valid: {storage_class_json}')

    return root_aws_key


def _manifest_proc(src_dir: str, folder_names, skip_name, root_key: str, single_project_bool: bool, aws_files,
                   for_snowball: bool, json_log: aws_upload.logs.Logging) -> tuple:
    """
    Process to generate manifest
    Args:
        src_dir: Source directory where files are located
        folder_names: Names of folders to only include in manifest
        skip_name: Ignores file path if contents of string included
        root_key: Root AWS key to be prepended to AWS key
        single_project_bool:
        aws_files: Existing aws files in repository
        for_snowball: Prepare manifest chunk processing for snowball limits
        json_log: json_log: JSON logging object

    Returns:

    """
    return_list = []
    file_count = 0
    skip_count = 0

    for path, dirs, files in walk(src_dir):
        if any(name in path for name in skip_name) is False:
            for file in files:
                json_log.add_object(file, Path(path).joinpath(file))
                file_count += 1
                gen_manifest = False

                try:
                    part_key = ManGen.create_file_aws_part_key(src_dir, path, root_key, single_project_bool)
                except IndexError:
                    json_log.add_object_check(file, 'processing', 'skipped', 'In root directory')
                    continue
                if any(project in path for project in folder_names) and folder_names:
                    gen_manifest = True
                elif not folder_names:
                    gen_manifest = True

                if gen_manifest:
                    # Exclude files existing in aws
                    if Path(part_key).joinpath(file).as_posix() not in set(aws_files):
                        return_list.append([part_key, Path(path).joinpath(file), for_snowball])
                    else:
                        json_log.add_object_check(file, 'processing', 'skipped', 'Exists in AWS')

                        skip_count += 1
                else:
                    skip_count += 1
    return return_list, file_count, skip_count


@click.command()
# Location of files to be included in manifest
@click.argument('source_directory', required=True, nargs=1, type=click.Path(exists=True))
# Destination where manifest will be written to
@click.argument('write_path', required=True, nargs=1, type=click.Path(exists=True))
# Name of AWS bucket e.g. archive-logs
@click.argument('aws_bucket_name', required=True, nargs=1, type=click.STRING)
# Name of storage json which includes storage classes details
@click.argument('storage_class_json', required=True, nargs=1, type=click.STRING)
@click.option('--include_folder_csv', '-icsv', nargs=1, type=click.Path(exists=True), default=None,
              help='CSV file with folder names to be included in manifest, used to filter out unwanted projects to be uploaded to AWS')
@click.option('--root_aws_key', '-rkey', nargs=1, required=True, type=click.STRING,
              help='First part of AWS key, this will be added to the front of the AWS key to specify location in bucket e.g. aerial/2020')
@click.option('--aws_inventory_csv', '-awscsv', nargs=1, type=click.Path(exists=True), multiple=True,
              help='Path to AWS inventory gzipped csv, used to skip files that already exist')
@click.option('--num_manifests', '-nman', default=1, type=click.INT,
              help="Number of manifest files to split output between")
@click.option('--num_processes', '-nproc', default=int(mp.cpu_count() / 2), type=click.INT,
              help="Number of threads to put into a processing pool")
@click.option('--single_project', '-sproj', is_flag=True, default=False,
              help="Generates manifest for everything in source directory, use if src_dir points directly to folder that has contents wanted for upload")
@click.option('--skip_name', '-sname', multiple=True, type=click.STRING, help="Name of folders to skip")
@click.option('--for_snowball', '-for_snow', is_flag=True, default=False,
              help="Generate manifest file to account for snowball max file sizes")
def main(source_directory, write_path, aws_bucket_name, root_aws_key, storage_class_json,
          include_folder_csv, aws_inventory_csv, num_manifests, num_processes,
          single_project, skip_name, for_snowball):
    """
    Args:
        source_directory - Source path of data to be processed into manifests;
        write_path - Destination path for manifest file(s);
        aws_bucket_name - S3 bucket name (case sensitive);
    """

    man_id = Path(source_directory).stem

    # JSON log setup
    log_path = '/scratch/lsi/datamv/aws_manifests/manifest_logs'
    Path(log_path).mkdir(exist_ok=True, parents=True)
    log_name = f"{dt.now().strftime('%Y-%m-%d')}_{man_id}_manifest-log.json"

    json_log = aws_upload.logs.Logging()
    dependency_list = [["aws_upload", metadata.version("aws_upload"), ["aws_upload.logs", "aws_upload.upload.manifestgenerator"]],
                       ["click", metadata.version("click")], ["pandas", metadata.version("pandas")],
                       ["numpy", metadata.version("numpy")]]
    variable_list = [["source_directory", source_directory], ["write_path", write_path],
                     ["aws_bucket_name", aws_bucket_name],
                     ["storage_class_json", storage_class_json], ["include_folder_csv", include_folder_csv],
                     ["root_aws_key", root_aws_key],
                     ["aws_inventory_csv", aws_inventory_csv], ["num_manifests", num_manifests],
                     ["num_processes", num_processes],
                     ["single_project", single_project], ["skip_name", skip_name]]
    json_log.add_script_bulk("manifest_gen_main.py", metadata.version("aws_upload"), dependency_list, variable_list)
    json_log.output_json(log_path, log_name)

    # Combine logging details into dictionary to pass into functions
    log_input = {
        "json_logger": json_log,
        "log_name": log_name
    }

    # Validate inputs
    root_aws_key = _input_validation(root_aws_key, storage_class_json)

    # Checks to ensure correct inputs
    if any(isinstance(element, list) for element in aws_inventory_csv):
        if isinstance(aws_inventory_csv[0], list):
            aws_inventory_csv = aws_inventory_csv[0]
        else:
            raise Exception('Inconsistent inputs for --inventory_csv input')

    if any(isinstance(element, list) for element in skip_name):
        if isinstance(skip_name[0], list):
            skip_name = skip_name[0]
        else:
            raise Exception('Inconsistent inputs for --skip_name input')

    # Get dataframe of aws inventory csvs and get aws key column
    try:
        existing_aws_files = Misc.concat_dataframes(aws_inventory_csv)['Key'].tolist()
    except TypeError:
        existing_aws_files = []

    if include_folder_csv is not None:
        folder_names = pd.read_csv(include_folder_csv).iloc[:, 0].tolist()
    else:
        folder_names = []

    # Generate manifest details for files
    mp_inputs, file_count, skip_count = _manifest_proc(source_directory, folder_names, skip_name, root_aws_key,
                                                       single_project, existing_aws_files, for_snowball, json_log)
    json_log.output_json(log_path, log_name)
    # Multiprocess generation of details to be put inside manifest JSON
    result_list = []
    pool = mp.Pool(processes=num_processes)
    r = pool.starmap_async(ManGen._pop_man_file, mp_inputs, callback=result_list.append)
    r.wait()
    pool.close()
    pool.join()

    error_files = 0
    processed_files = 0
    # Create manifest file object
    manifest_obj = ManifestFile(source_directory, aws_bucket_name, storage_class_json)
    # Add file details to manifest object
    for file_path, file_name, size, checksum, chunks, aws_key, status, manifest_details in result_list[0]:
        # path, file, size, b64md5_checksum, md5_chunks, key
        if size is None and checksum is None and chunks is None and aws_key is None:
            json_log.add_object_process(file_name, 'manifest_details', status, manifest_details)
            error_files += 1
        else:
            manifest_obj.add_file(file_path, file_name, size, checksum, chunks, aws_key)
            json_log.add_object_process(file_name, 'manifest_details', status, manifest_details)
            processed_files += 1

    json_log.add_summary_process('processed_files',
                                 {'total_files': file_count, 'processed_files': processed_files,
                                  'skipped_files': skip_count, 'error_files': error_files})

    manifest_obj.write_manifest_json(write_path, num_manifests, log_input, man_id)

    json_log.output_json(log_path, log_name)


if __name__ == '__main__':
    main()
