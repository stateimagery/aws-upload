# aws-upload

## Overview

Scripts developed by the State Imagery Team to support the upload of data to AWS S3 storage. Key features include
checksum validation, multiprocessing support, and logging information. For re-use some modification of default values
(e.g. S3 buckets) may be required.

For further details, please contact John Tasker (john.tasker@resources.qld.gov.au)

## License
[GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)

## Project status
Updates may be published as more of our existing codebase is published publicly.